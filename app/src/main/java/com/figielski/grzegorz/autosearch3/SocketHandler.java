package com.figielski.grzegorz.autosearch3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class SocketHandler {

    private SocketChannel socketChannel = null;
    //private int port = 1337;

    public void receiveConnection() throws IOException {
        createSocketChannel(1337);
        ObjectInputStream objectInputStream = new ObjectInputStream(socketChannel.socket().getInputStream());
        //TestClass obj = null;
        int id = 0;
        try {
            id = (int) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Otrzymano obiekt: " + id + " ");
        socketChannel.close();
    }
    private void createSocketChannel(int port) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));
        socketChannel = serverSocketChannel.accept();

        //System.out.println("Połączono z " + socketChannel.getLocalAddress());
    }
}
