package com.figielski.grzegorz.autosearch3;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import serversocketdemo.Network_auto;
import serversocketdemo.ProductAutomaton;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView textView;

        final String defaultHostname = "192.168.0.150";
        textView = new TextView(this);
        textView.setText("hostname / IP:");
        linearLayout.addView(textView);
        final EditText hostnameEditText = new EditText(this);
        hostnameEditText.setText(defaultHostname);
        hostnameEditText.setSingleLine(true);
        linearLayout.addView(hostnameEditText);

        textView = new TextView(this);
        textView.setText("port:");
        linearLayout.addView(textView);
        final EditText portEditText = new EditText(this);
        portEditText.setText("1337");
        portEditText.setSingleLine(true);
        linearLayout.addView(portEditText);

        /*textView = new TextView(this);
        textView.setText("data to send:");
        linearLayout.addView(textView);
        final EditText dataEditText = new EditText(this);
        dataEditText.setText(String.format("1", defaultHostname));
        linearLayout.addView(dataEditText);*/

        final TextView replyTextView = new TextView(this);
        final ScrollView replyTextScrollView = new ScrollView(this);
        replyTextScrollView.addView(replyTextView);

        final Button button = new Button(this);
        button.setText("contact server");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.setEnabled(false);
                new MyAsyncTask(MainActivity.this, replyTextView, button).execute(
                        hostnameEditText.getText().toString(),
                        portEditText.getText().toString());//,
                        //dataEditText.getText().toString());

            }
        });
        linearLayout.addView(button);

        /*textView = new TextView(this);
        textView.setText("output:");
        linearLayout.addView(textView);
        linearLayout.addView(replyTextScrollView);*/

        this.setContentView(linearLayout);
    }

    private class MyAsyncTask extends AsyncTask<String, Void, String> {
        public ProgressDialog mProgressDialog;
        Activity activity;
        Button button;
        TextView textView;
        IOException ioException;
        MyAsyncTask(Activity activity, TextView textView, Button button) {
            super();
            this.activity = activity;
            this.textView = textView;
            this.button = button;
            this.ioException = null;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(activity);
            mProgressDialog.setTitle("Please wait...");
            mProgressDialog.setMessage("Solving request");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            StringBuilder sb = new StringBuilder();
            try {
                Socket socket = new Socket(
                        params[0],
                        Integer.parseInt(params[1]));
                /*OutputStream out = socket.getOutputStream();
                out.write(params[2].getBytes());
                InputStream in = socket.getInputStream();*/
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                //outputStream.write(params[2].getBytes());
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                /*byte buf[] = new byte[1024];
                int nbytes;*/
                //while(socket.isConnected()){
                    String strjing = (String) objectInputStream.readObject();
                    Gson gson = new Gson();
                    Network_auto networkAuto = gson.fromJson(strjing, Network_auto.class);
                ProductAutomaton productAutomaton;
                String automaton_string;
                    while(socket.isConnected()){
                        automaton_string = (String) objectInputStream.readObject();
                        productAutomaton = gson.fromJson(automaton_string, ProductAutomaton.class);
                        productAutomaton = networkAuto.solveDevice(productAutomaton);
                        outputStream.flush();
                        outputStream.writeObject(gson.toJson(productAutomaton));
                    }
                //}
                socket.close();
            } catch(IOException e) {
                this.ioException = e;
                return "error";
            } catch (ClassNotFoundException e) {
                            e.printStackTrace();
            }
            return sb.toString();
        }
        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.dismiss();
            /*if (this.ioException != null) {
                new AlertDialog.Builder(this.activity)
                        .setTitle("An error occurrsed")
                        .setMessage(this.ioException.toString())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } else {
                this.textView.setText(result);
            }*/
            this.button.setEnabled(true);
        }
    }
}