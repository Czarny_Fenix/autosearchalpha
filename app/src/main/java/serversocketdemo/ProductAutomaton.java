package serversocketdemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class ProductAutomaton implements Serializable
{
    private static final long serialVersionUID = 1;
    private static ProductAutomaton Instance = null;
    public ArrayList<StatesNode> nodes;
    int numberOfNodes;
    HashSet<Integer> usedTransitionLabels;
    HashSet<Integer> usedStatesLabels;
    Network_auto parentNetwork;

    static ProductAutomaton getInstance(Network_auto network_auto) {
        if(Instance == null){
            Instance = new ProductAutomaton(network_auto);
        }
        return Instance;
    }

    public ProductAutomaton(Network_auto network)
    {
        this.nodes = new ArrayList<>();
        this.numberOfNodes = 0;
        this.usedStatesLabels = new HashSet<>();
        this.usedTransitionLabels = new HashSet<>();
        this.parentNetwork = network;
    }

    public void add(ArrayList<Integer> state)
    {
        StatesNode newNode = new StatesNode(state, ++this.numberOfNodes);
        this.nodes.add(newNode);
        for (Integer n : state)
        {
            this.usedStatesLabels.add(n);
        }
    }

    public boolean contains(ArrayList<Integer> state)
    {
        for(StatesNode statesNode : nodes)
        {
            //System.out.println(state.size() + " " + statesNode.statesLabels.size());
            if(statesNode.statesLabels.size() == state.size() && statesNode.statesLabels.containsAll(state))
            {
                return true;
            }
        }

        return false;
    }

    public void addTransition(ArrayList<Integer> state, ArrayList<Integer> newState, int transisiotnLabel)
    {
        StatesNode stateFrom = this.getState(state);
        if(!stateFrom.containsNeighbour(newState))
        {
            StatesNode stateTo = this.getState(newState);
            stateFrom.addNeighbour(stateTo, transisiotnLabel);
            this.usedTransitionLabels.add(transisiotnLabel);
        }
    }

    private StatesNode getState(ArrayList<Integer> state)
    {
        for(StatesNode statesNode : this.nodes)
        {
            if(statesNode.statesLabels.size() == state.size() && statesNode.statesLabels.containsAll(state))
            {
                return statesNode;
            }
        }

        return null;
    }

    public void print()
    {
        for(StatesNode node : this.nodes)
        {
            System.out.println(node + "\n\n");
        }
    }

    public void printStatistics()
    {
        this.parentNetwork.countTransitions();
        this.parentNetwork.countNodes();
        ArrayList<Integer> netTransitions = new ArrayList<>(Arrays.asList(this.parentNetwork.getTransitionLabels()));
        ArrayList<Integer> netLabels = new ArrayList<>(Arrays.asList(this.parentNetwork.getNodeLabels()));
        ArrayList<Integer> unusedTransitions = new ArrayList<>();
        ArrayList<Integer> unusedLabels = new ArrayList<>();

        for(Integer n : netTransitions)
        {
            if (!this.usedTransitionLabels.contains(n))
            {
                unusedTransitions.add(n);
            }
        }

        for(Integer n : netLabels)
        {
            if (!this.usedStatesLabels.contains(n))
            {
                unusedLabels.add(n);
            }
        }

        StringBuilder printReady = new StringBuilder();
        printReady.append("Unused states: ");
        if(unusedLabels.isEmpty())
        {
            printReady.append("---");
        }
        else
        {
            for(Integer n : unusedLabels)
            {
                printReady.append(n).append(" ");
            }
        }
        printReady.append("\nUnused transitions: ");
        if(unusedTransitions.isEmpty())
        {
            printReady.append("---");
        }
        else
        {
            for(Integer n : unusedTransitions)
            {
                printReady.append(n).append(" ");
            }
        }
        printReady.append("\n");

        System.out.println(printReady.toString());
    }
}
