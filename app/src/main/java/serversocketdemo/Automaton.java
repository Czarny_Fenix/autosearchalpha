package serversocketdemo;

import java.io.Serializable;
import java.util.ArrayList;

public class Automaton implements Serializable
{
    private static final long serialVersionUID = 1;
    ArrayList<Node> nodes;
    int automatonCapacity;
    int start, end, first, last;
    Node current;

    public Automaton(int start, int end)
    {
        this.start = start;
        this.end = end;
        this.first = start;
        this.last = end;
        this.nodes = new ArrayList<>();
        this.automatonCapacity = end - start + 1;
        for (int i = 0; i < this.automatonCapacity; i++)
        {
            this.nodes.add(new Node(i + this.start));
        }
    }

    public void addTransition(int nodeLabelStart, int nodeLabelEnd, int transitionLabel)
    {
        Node start = this.nodes.get(nodeLabelStart - this.start);
        Node end = this.nodes.get(nodeLabelEnd - this.start);
        start.addNeighbour(end.label, transitionLabel);
    }

    public void setCurrent(int nodeLabel)
    {
        this.current = this.nodes.get(nodeLabel - this.start);
    }

    public void printAutomaton()
    {
        for (Node n : this.nodes)
        {
            System.out.println(n + "\n");
        }
    }

    public boolean transitionExists(int transitionLabel)
    {
        for(Node n : this.nodes)
        {
            for(Neighbour ne : n.neighbours)
            {
                if (ne.transitionLabel == transitionLabel)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isTransitionAdjToCurrent(int transitionLabel)
    {
        for(Neighbour ne : this.current.neighbours)
        {
            if(ne.transitionLabel == transitionLabel)
            {
                return true;
            }
        }

        return false;
    }

    public void makeTransition(int transitionLabel)
    {
        //with assumption that transition check was already done, make transition where it is adjacent to current node
        for(Neighbour n : this.current.neighbours)
        {
            if(n.transitionLabel == transitionLabel)
            {
                setCurrent(n.node);
            }
        }
    }

    public boolean isReached()
    {
        if(this.current.label == this.last)
            return true;
        else
            return false;
    }

    public void setNodeInitial(int nodeLabel)
    {
        this.nodes.get(nodeLabel - this.start).setInital();
    }

    public void setNodeFinal(int nodeLabel)
    {
        this.nodes.get(nodeLabel - this.start).setFinal();
    }

    public int getStart() {return start;}
    public int getEnd() {return end;}
    public ArrayList<Node> getNodes() {return nodes;}
}
