package serversocketdemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

public class StatesNode implements Serializable
{
    ArrayList<Integer> statesLabels;
    LinkedList<StatesNeighbour> statesNeighbours;
    int globalHash;
    int workFlag;

    public StatesNode(ArrayList<Integer> statesLabel, int globalHash)
{
    this.statesLabels = statesLabel;
    this.globalHash = globalHash;
    this.statesNeighbours = new LinkedList<>();
    workFlag=0;
}

    public StatesNode(ArrayList<Integer> statesLabel, int globalHash, int workFlag)
    {
        this.statesLabels = statesLabel;
        this.globalHash = globalHash;
        this.statesNeighbours = new LinkedList<>();
        this.workFlag=workFlag;
    }

    public void addNeighbour(StatesNode statesNeighbour, int transitionLabel)
    {
        StatesNeighbour n = new StatesNeighbour(statesNeighbour, transitionLabel);
        this.statesNeighbours.add(n);
    }

    public boolean containsNeighbour(ArrayList<Integer> neighbour)
    {
        for(StatesNeighbour n : this.statesNeighbours)
        {
            if(n.stateNode.statesLabels.size() == neighbour.size() && n.stateNode.statesLabels.containsAll(neighbour))
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder("StatesNode{\nlabel:\n");
        stringBuilder.append(" #" + this.globalHash + ":");

        for (Integer i : this.statesLabels)
        {
            stringBuilder.append(" " + i);
        }
        stringBuilder.append("\n");

        for (StatesNeighbour n : this.statesNeighbours)
        {
            stringBuilder.append("connected to\n");
            stringBuilder.append(" #" + n.stateNode.globalHash + ":");
            for (Integer i : n.stateNode.statesLabels)
            {
                stringBuilder.append(" " + i);
            }
            stringBuilder.append("\nthrough " + n.transitionLabel + "\n");
        }
        stringBuilder.append("}");

        return stringBuilder.toString();
    }

    public ArrayList<Integer> getStatesLabels() {
        return statesLabels;
    }

    public LinkedList<StatesNeighbour> getStatesNeighbours() {
        return statesNeighbours;
    }

    public int getGlobalHash() {
        return globalHash;
    }

    public int getWorkFlag() {
        return workFlag;
    }

    public void setWorkFlag(int workFlag) {
        this.workFlag = workFlag;
    }

}
