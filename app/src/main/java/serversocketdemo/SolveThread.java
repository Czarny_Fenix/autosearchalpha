package serversocketdemo;

public class SolveThread extends Thread {

    Network_auto network;
    ProductAutomaton productAutomaton;

    public SolveThread(Network_auto network, ProductAutomaton productAutomaton) {
        this.network = network;
        this.productAutomaton = productAutomaton;
    }

    public void run() {
        while(!network.checkForEnd(productAutomaton)){
            productAutomaton=network.solveWglab(productAutomaton);
        }
    }
}
