package serversocketdemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

public class Node implements Serializable
{
    private static final long serialVersionUID = 1;
    int label;
    LinkedList<Neighbour> neighbours;
    boolean _inital, _final;

    public Node(int label)
    {
        this.label = label;
        this._inital = this._final = false;
        this.neighbours = new LinkedList<>();
    }

    public void addNeighbour(int neighbour, int transitionLabel)
    {
        Neighbour n = new Neighbour(neighbour, transitionLabel);
        this.neighbours.add(n);
    }

    public void setInital()
    {
        this._inital = true;
    }

    public void setFinal()
    {
        this._final = true;
    }

    public ArrayList<int[]> getTransitions() {
        ArrayList<int[]> transitions = new ArrayList<>();
        for(Neighbour n : neighbours) {
            int[] i = new int[3];
            i[0] = this.label;
            i[1] = n.node;
            i[2] = n.transitionLabel;

            transitions.add(i);
         }

         return transitions;
    }

    public int getLabel() {
        return label;
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder("Node{\nlabel: ");
        stringBuilder.append(this.label + "\n");
        for(Neighbour n : neighbours)
        {
            stringBuilder.append("connected to " + n.node + " through " + n.transitionLabel + "\n");
        }
        stringBuilder.append("}");

        return stringBuilder.toString();
    }
}
