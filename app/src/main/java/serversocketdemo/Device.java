package serversocketdemo;

import java.net.SocketAddress;

public class Device {

    private int id;
    SocketAddress ip;

    public Device(int id, SocketAddress ip) {
        this.id = id;
        this.ip = ip;
    }

    public int getId() {
        return id;
    }

    public SocketAddress getIp() {
        return ip;
    }
}
