package serversocketdemo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class Network_auto implements Serializable
{
    private static final long serialVersionUID = 1;
    LinkedList<Automaton> net;
    int nTransitions;
    Integer transitionLabels[];
    Integer nodeLabels[];

    public Network_auto()
    {
        this.net = new LinkedList<>();
    }

    public void addAutomaton(Automaton newAutomaton)
    {
        this.net.add(newAutomaton);
    }

    public void countTransitions()
    {
        HashSet<Integer> uniqueTransitions = new HashSet<>();
        for (Automaton a : this.net)
        {
            for (Node n : a.nodes)
            {
                for(Neighbour ne : n.neighbours)
                {
                    uniqueTransitions.add(ne.transitionLabel);
                }
            }
        }

        this.nTransitions = uniqueTransitions.size();
        this.transitionLabels = uniqueTransitions.toArray(new Integer[uniqueTransitions.size()]);
    }

    public void countNodes()
    {
        HashSet<Integer> uniqueNodes = new HashSet<>();
        for(Automaton a : this.net)
        {
            for(Node n : a.nodes)
            {
                uniqueNodes.add(n.label);
            }
        }

        this.nodeLabels = uniqueNodes.toArray(new Integer[uniqueNodes.size()]);
    }

    public boolean checkTransition(int transitionLabel)
    {
        for(Automaton automaton : net)
        {
            if(automaton.transitionExists(transitionLabel))
            {
                if(!automaton.isTransitionAdjToCurrent(transitionLabel))
                    return false;
            }
        }

        return true;
    }

    public void makeTransition(int transitionLabel)
    {
        for (Automaton automaton : this.net)
        {
            automaton.makeTransition(transitionLabel);
        }
    }

    public void setState(ArrayList<Integer> state)
    {
        int i = 0;
        for(Automaton a : this.net)
        {
            a.setCurrent(state.get(i++));
        }
    }

    public ArrayList<Integer> getState()
    {
        ArrayList<Integer> state = new ArrayList<>();
        for(Automaton a : this.net)
        {
            state.add(a.current.label);
        }

        return state;
    }

    public ProductAutomaton solve()
    {
        ArrayList<Integer> state;
        Stack<ArrayList<Integer>> stateStack = new Stack<>();
        ProductAutomaton productAutomaton = new ProductAutomaton(this);

        //initial state
        state = this.getState();

        productAutomaton.add(state);
        stateStack.push(state);

        LinkedList<Integer> transitionQueue = new LinkedList<>();
        int transition;
        ArrayList<Integer> newState;
        while(!stateStack.empty())
        {
            state = stateStack.pop();
            this.setState(state);
            for(Automaton a : this.net)
            {
                for(Neighbour n : a.current.neighbours)
                {
                    if(!transitionQueue.contains(n.transitionLabel))
                    {
                        transitionQueue.addLast(n.transitionLabel);
                    }
                }
            }
            while(!transitionQueue.isEmpty())
            {
                transition = transitionQueue.poll();
                if(this.checkTransition(transition))
                {
                    this.makeTransition(transition);
                    newState = this.getState();
                    if(productAutomaton.contains(newState))
                    {
                        productAutomaton.addTransition(state, newState, transition);
                    }
                    else
                    {
                        stateStack.push(newState);
                        productAutomaton.add(newState);
                        productAutomaton.addTransition(state, newState, transition);
                    }
                    this.setState(state); // state before transition
                }
            }
        }

        return productAutomaton;
    }

    public ProductAutomaton solveMultiDevice()
    {
        ArrayList<Integer> state;
        Stack<ArrayList<Integer>> stateStack = new Stack<>();
        ProductAutomaton productAutomaton = new ProductAutomaton(this);

        //initial state
        state = this.getState();

        productAutomaton.add(state);
        stateStack.push(state);

        LinkedList<Integer> transitionQueue = new LinkedList<>();
        int transition;
        ArrayList<Integer> newState;
        while(!stateStack.empty())
        {
            state = stateStack.pop();
            this.setState(state);
            for(Automaton a : this.net)
            {
                for(Neighbour n : a.current.neighbours)
                {
                    if(!transitionQueue.contains(n.transitionLabel))
                    {
                        transitionQueue.addLast(n.transitionLabel);
                    }
                }
            }
            while(!transitionQueue.isEmpty())
            {
                transition = transitionQueue.poll();
                if(this.checkTransition(transition))
                {
                    this.makeTransition(transition);
                    newState = this.getState();
                    if(productAutomaton.contains(newState))
                    {
                        productAutomaton.addTransition(state, newState, transition);
                    }
                    else
                    {
                        stateStack.push(newState);
                        productAutomaton.add(newState);
                        productAutomaton.addTransition(state, newState, transition);
                    }
                    this.setState(state); // state before transition
                }
            }
        }

        return productAutomaton;
    }

    public LinkedList<Automaton> getNet() {return this.net;}

    public Integer[] getTransitionLabels()
    {
        return transitionLabels;
    }

    public Integer[] getNodeLabels()
    {
        return nodeLabels;
    }

    public int dajRobote(ProductAutomaton mainAutomaton){
        int i;
        int flag_end = -1;
        for(i=0;i<mainAutomaton.nodes.size();i++){
            if(mainAutomaton.nodes.get(i).getWorkFlag()==0){
                mainAutomaton.nodes.get(i).setWorkFlag(3);
                flag_end++;
                break;
            }
        }
        //return mainAutomaton.nodes.get(i);
        if(flag_end==-1){
            return flag_end;
        } else {
            return i;
        }
        //return i;
    }

    public void przyjetoDyspozycje(ProductAutomaton productAutomaton, int i){
        productAutomaton.nodes.get(i).setWorkFlag(2);
    }

    public ProductAutomaton zlozWyniki(ProductAutomaton mainAutomaton, ProductAutomaton partSolvedAutomaton, Integer node){
        int flaga_obecnosci;




        for(int i=0;i<partSolvedAutomaton.nodes.size();i++){
            if(!mainAutomaton.contains(partSolvedAutomaton.nodes.get(i).statesLabels)){
                mainAutomaton.add(partSolvedAutomaton.nodes.get(i).statesLabels);
            }
        }
        for (int i=0;i<partSolvedAutomaton.nodes.get(node).statesNeighbours.size();i++){
            if(!mainAutomaton.nodes.get(node).containsNeighbour(partSolvedAutomaton.nodes.get(node).statesNeighbours.get(i).stateNode.statesLabels)){
                mainAutomaton.addTransition(mainAutomaton.nodes.get(node).statesLabels,partSolvedAutomaton.nodes.get(node).statesNeighbours.get(i).stateNode.statesLabels,partSolvedAutomaton.nodes.get(node).statesNeighbours.get(i).transitionLabel);
            }
        }
        mainAutomaton.nodes.get(node).setWorkFlag(1);


        /*this.makeTransition(neighbour.transitionLabel);
        ArrayList<Integer> newState = this.getState();
        if(mainAutomaton.contains(newState))
        {
            mainAutomaton.addTransition(mainAutomaton.nodes.get(i).statesLabels, newState, neighbour.transitionLabel);
        } else{
            mainAutomaton.add(newState);
            mainAutomaton.addTransition(mainAutomaton.nodes.get(i).statesLabels, newState, neighbour.transitionLabel);
        }*/




        //mainAutomaton.nodes.set(node, new StatesNode(mainAutomaton.nodes.get(node).getStatesLabels(),mainAutomaton.nodes.get(node).getGlobalHash(),1));
        /*for(int j=0;j<partSolvedAutomaton.nodes.size();j++){
            if(partSolvedAutomaton.nodes.get(j).getWorkFlag()==3){
                mainAutomaton.nodes.get(node) = partSolvedAutomaton.nodes.get(j);
                mainAutomaton.nodes.get(node).setWorkFlag(1);
                break;
            }
        }*/
        /*for(int i=0;i<partSolvedAutomaton.nodes.size();i++){
            for(int j=0;j<mainAutomaton.nodes.size();j++){
                for(int k=0;k<mainAutomaton.nodes.get(j).statesNeighbours.size();k++){
                    if(!mainAutomaton.contains(partSolvedAutomaton.nodes.get(i).statesLabels)){
                        mainAutomaton.add(partSolvedAutomaton.nodes.get(i).statesLabels);
                        for(StatesNeighbour neighbour : partSolvedAutomaton.nodes.get(i).statesNeighbours){
                            mainAutomaton.addTransition((partSolvedAutomaton.nodes.get(i).statesLabels, neighbour.stateNode.statesLabels, neighbour.transitionLabel));
                        }
                    } else {
                        if(mainAutomaton.nodes.get(j).statesNeighbours.get(k).stateNode)
                    }
                }
            }
        }*/

        /*for(int i=0;i<partSolvedAutomaton.nodes.size();i++){
            for(int j=0;j<mainAutomaton.nodes.get(i).statesNeighbours.size();j++){
                if(!mainAutomaton.contains(partSolvedAutomaton.nodes.get(i).statesLabels)){
                    mainAutomaton.add(partSolvedAutomaton.nodes.get(i).statesLabels);
                    for(StatesNeighbour neighbour : partSolvedAutomaton.nodes.get(i).statesNeighbours){
                        mainAutomaton.addTransition(partSolvedAutomaton.nodes.get(i).statesLabels, neighbour.stateNode.statesLabels, neighbour.transitionLabel);
                    }
                    //mainAutomaton.addTransition(partSolvedAutomaton.nodes.get(i).statesLabels, partSolvedAutomaton.nodes.get(i).statesNeighbours.get(i).stateNode.statesLabels, partSolvedAutomaton.nodes.get(i).statesNeighbours.get(i).transitionLabel);
                } else{
                    //for(int k=0;k<mainAutomaton.nodes.size();k++){
                    for(StatesNeighbour neighbour : partSolvedAutomaton.nodes.get(i).statesNeighbours){
                        mainAutomaton.addTransition(partSolvedAutomaton.nodes.get(i).statesLabels, neighbour.stateNode.statesLabels, neighbour.transitionLabel);
                    }
                    //}
                }*/

                    /*for(StatesNeighbour neighbour : partSolvedAutomaton.nodes.get(i).statesNeighbours){
                    if(mainAutomaton.nodes.get(i).containsNeighbour(neighbour)){

                    }
                    mainAutomaton.addTransition(partSolvedAutomaton.nodes.get(i).statesLabels, neighbour.stateNode.statesLabels, neighbour.transitionLabel);
                }*/
            /*}*/
            /*if(!mainAutomaton.contains(partSolvedAutomaton.nodes.get(i).statesLabels)){
                mainAutomaton.add(partSolvedAutomaton.nodes.get(i).statesLabels);
            } else {
                mainAutomaton.add(partSolvedAutomaton.nodes.get(i).statesLabels);
                mainAutomaton.addTransition(partSolvedAutomaton.nodes.get(i).statesLabels, partSolvedAutomaton.nodes.get(i).statesNeighbours.get(i).stateNode.statesLabels, partSolvedAutomaton.nodes.get(i).statesNeighbours.get(i).transitionLabel);
            }*/
        /*}*/

        /*for(int i=0;i<partSolvedAutomaton.nodes.size();i++){
            flaga_obecnosci=0;
            for(int j=0;j<mainAutomaton.nodes.size();j++){
                for(int k=0;k<mainAutomaton.nodes.get(j).statesLabels.size();k++){
                    if(partSolvedAutomaton.nodes.get(i).statesLabels.get(k)!=mainAutomaton.nodes.get(j).statesLabels.get(k)){
                        break;
                    } else if(k==mainAutomaton.nodes.get(j).statesLabels.size()-1){
                        flaga_obecnosci=1;
                    }
                }
                if(flaga_obecnosci==1){
                    //mainAutomaton.addTransition();
                    break;
                }
                /*if(partSolvedAutomaton.nodes.get(i).statesLabels==mainAutomaton.nodes.get(j).statesLabels){
                    flaga_obecnosci=1;
                    break;
                }*/
            /*}
            if(flaga_obecnosci==0){
                mainAutomaton.nodes.add(partSolvedAutomaton.nodes.get(i));
            }
        }*/
        return mainAutomaton;
    }

    public ProductAutomaton initState(ProductAutomaton productAutomaton){
        ArrayList<Integer> state;

        //initial state
        state = this.getState();

        productAutomaton.add(state);
        return productAutomaton;
    }

    public ProductAutomaton solveWglab(ProductAutomaton mainAutomaton){
        for(int i=0; i<mainAutomaton.nodes.size();i++){
            if(mainAutomaton.nodes.get(i).getWorkFlag()==1){
                continue;
            } else if(mainAutomaton.nodes.get(i).getWorkFlag()==0){
                mainAutomaton.nodes.get(i).setWorkFlag(2);
                this.setState(mainAutomaton.nodes.get(i).statesLabels);
                for(Automaton automaton : this.net){
                    for(Neighbour neighbour : automaton.current.neighbours){
                        this.setState(mainAutomaton.nodes.get(i).statesLabels);
                        if(this.checkTransition(neighbour.transitionLabel)){
                            this.makeTransition(neighbour.transitionLabel);
                            ArrayList<Integer> newState = this.getState();
                            if(mainAutomaton.contains(newState))
                            {
                                mainAutomaton.addTransition(mainAutomaton.nodes.get(i).statesLabels, newState, neighbour.transitionLabel);
                            } else{
                                mainAutomaton.add(newState);
                                mainAutomaton.addTransition(mainAutomaton.nodes.get(i).statesLabels, newState, neighbour.transitionLabel);
                            }
                        }
                    }
                }
                /*for(Automaton automaton : this.net){
                    for(Neighbour neighbour : automaton.current.neighbours){
                        if(this.checkTransition(neighbour.transitionLabel)){

                            this.makeTransition(transition);
                            ArrayList<Integer> newState = this.getState();
                            if(mainAutomaton.contains(newState))
                            {
                                mainAutomaton.addTransition(state, newState, transition);
                            } else{
                                mainAutomaton.add(newState);
                                mainAutomaton.addTransition(state, newState, transition);
                            }
                        }
                    }
                }*/
                mainAutomaton.nodes.get(i).setWorkFlag(1);
                break;
            }
        }
        return mainAutomaton;
    }

    public ProductAutomaton solveDevice(ProductAutomaton mainAutomaton){
        for(int i=0;i<mainAutomaton.nodes.size();i++){
            if(mainAutomaton.nodes.get(i).getWorkFlag()==3){
                this.setState(mainAutomaton.nodes.get(i).statesLabels);
                for(Automaton automaton : this.net){
                    for(Neighbour neighbour : automaton.current.neighbours){
                        this.setState(mainAutomaton.nodes.get(i).statesLabels);
                        if(this.checkTransition(neighbour.transitionLabel)){
                            this.makeTransition(neighbour.transitionLabel);
                            ArrayList<Integer> newState = this.getState();
                            if(mainAutomaton.contains(newState))
                            {
                                mainAutomaton.addTransition(mainAutomaton.nodes.get(i).statesLabels, newState, neighbour.transitionLabel);
                            } else{
                                mainAutomaton.add(newState);
                                mainAutomaton.addTransition(mainAutomaton.nodes.get(i).statesLabels, newState, neighbour.transitionLabel);
                            }
                        }
                    }
                }
                /*for(Automaton automaton : this.net){
                    for(Neighbour neighbour : automaton.current.neighbours){
                        if(this.checkTransition(neighbour.transitionLabel)){

                            this.makeTransition(transition);
                            ArrayList<Integer> newState = this.getState();
                            if(mainAutomaton.contains(newState))
                            {
                                mainAutomaton.addTransition(state, newState, transition);
                            } else{
                                mainAutomaton.add(newState);
                                mainAutomaton.addTransition(state, newState, transition);
                            }
                        }
                    }
                }*/

                //mainAutomaton.nodes.get(i).setWorkFlag(1);

                /*for(Automaton automaton : this.net){
                    for(Neighbour neighbour : automaton.current.neighbours){
                        if(this.checkTransition(neighbour.transitionLabel)){
                            dopisz stan;
                        }
                    }
                }*/
                //mainAutomaton.nodes.get(i).setWorkFlag(1);
            }
        }
        return mainAutomaton;
    }

    public boolean checkForEnd(ProductAutomaton mainAutomaton){
        boolean flaga = true;
        for(int i=0;i<mainAutomaton.nodes.size();i++){
            if(mainAutomaton.nodes.get(i).getWorkFlag()!=1){
                flaga = false;
                break;
            }
        }
        return flaga;
    }

    private void flagManipulation(ProductAutomaton productAutomaton){
        for(int i=0;i<productAutomaton.nodes.size();i++){
            if(productAutomaton.nodes.get(i).getWorkFlag()==1){
                productAutomaton.nodes.get(i).setWorkFlag(0);
            }
        }
    }

}
