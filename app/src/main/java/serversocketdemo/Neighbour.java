package serversocketdemo;

import java.io.Serializable;

public class Neighbour implements Serializable
{
    private static final long serialVersionUID = 1;
    int node;
    int transitionLabel;

    public Neighbour(int neighbour, int transitionLabel)
    {
        this.node = neighbour;
        this.transitionLabel = transitionLabel;
    }
}
