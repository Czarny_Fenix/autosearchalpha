package serversocketdemo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketMaker extends Thread implements EndListener{
    Network_auto network_auto;
    ProductAutomaton productAutomaton;
    int clientNumber = 0;

    public void onEnd(ProductAutomaton productAutomaton, Network_auto network_auto){
        if(!network_auto.checkForEnd(productAutomaton)){
            return;
        } else {
            this.stop();
        }
    }

    public SocketMaker(Network_auto network, ProductAutomaton productAutomaton) {
        this.network_auto = network;
        this.productAutomaton = productAutomaton;
    }

    public void run() {
        try(ServerSocket socket = new ServerSocket(1337);){
            while(!network_auto.checkForEnd(productAutomaton)){
                new SocketHandler(socket.accept(),clientNumber++, network_auto, productAutomaton).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
