package serversocketdemo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Parser
{
    String fileName;

    public Parser(String fileName)
    {
        this.fileName = fileName;
    }

    public void readAutomatonFromFile()
    {
        ArrayList<String> lines = new ArrayList<>();
        String line;

        try(BufferedReader reader = new BufferedReader(new FileReader(this.fileName)))
        {
            while ((line = reader.readLine()) != null)
            {
                line = line.replaceAll("\t", "");
                if(line.equals("NETWORK"))
                {

                }
                else
                {
                    System.out.println("File corrupted");
                    break;
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        for (String s : lines)
        {
            System.out.println(s);
        }
    }
}
