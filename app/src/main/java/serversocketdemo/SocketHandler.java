package serversocketdemo;

import com.google.gson.Gson;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketHandler extends Thread{

    LinkedList<Device> devices;
    private SocketChannel socketChannel = null;
    private ObjectOutputStream outputStream;
    //private int port = 1337;

    public void receiveConnection() throws IOException {
        createSocketChannel(1337);
        ObjectInputStream objectInputStream = new ObjectInputStream(socketChannel.socket().getInputStream());
        //TestClass obj = null;
        int id = 0;
        try {
            id = (int) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Otrzymano obiekt: " + id + " ");
        //closeConnection();
    }

    public void closeConnection() throws IOException {
        socketChannel.close();
    }

    private void createSocketChannel(int port) throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));
        socketChannel = serverSocketChannel.accept();

        System.out.println("Połączono z " + socketChannel.getLocalAddress());
    }
    public void sendData(Network_auto network) throws IOException {
        outputStream = new ObjectOutputStream(socketChannel.socket().getOutputStream());
        outputStream.writeObject(network);
    }

    public ProductAutomaton recieveData() throws IOException {
        ProductAutomaton productAutomaton=null;
        ObjectInputStream objectInputStream = new ObjectInputStream(socketChannel.socket().getInputStream());
        try {
            productAutomaton = (ProductAutomaton) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return productAutomaton;
    }


        private Network_auto network;
        private Socket socket;
        private int clientNumber;
        private ProductAutomaton productAutomaton;

        public SocketHandler(Socket socket, int clientNumber, Network_auto network) {
            this.network = network;
            this.socket = socket;
            this.clientNumber = clientNumber;
            System.out.println("New client #" + clientNumber + " connected at " + socket);
        }

    public SocketHandler(Socket socket, int clientNumber, Network_auto network, ProductAutomaton productAutomaton) {
        this.network = network;
        this.socket = socket;
        this.clientNumber = clientNumber;
        this.productAutomaton = productAutomaton;
        System.out.println("New client #" + clientNumber + " connected at " + socket);
    }

        /**
         * Services this thread's client by first sending the client a welcome
         * message then repeatedly reading strings and sending back the capitalized
         * version of the string.
         */
        public void run() {
            try {
                ProductAutomaton productAutomatontemp;
                int statesNode;
                ObjectOutputStream outputStream;
                outputStream = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream inputStream;
                inputStream = new ObjectInputStream(socket.getInputStream());
                //outputStream.writeObject(network);
                Gson gson = new Gson();
                outputStream.flush();
                outputStream.writeObject(gson.toJson(network));
                while(!network.checkForEnd(productAutomaton)){
                    statesNode = network.dajRobote(productAutomaton);
                    if(statesNode==-1){
                        continue;
                    }
                    outputStream.writeObject(gson.toJson(productAutomaton));
                    network.przyjetoDyspozycje(productAutomaton, statesNode);
                    String strjing = (String) inputStream.readObject();
                    productAutomatontemp = gson.fromJson(strjing, ProductAutomaton.class);
                    network.zlozWyniki(productAutomaton, productAutomatontemp, statesNode);
                }
                //outputStream.writeObject(gson.toJson(productAutomaton));
                /*var in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                var out = new PrintWriter(socket.getOutputStream(), true);*/

                // Send a welcome message to the client.
                //System.out.println("Hello, you are client #" + clientNumber);

                // Get messages from the client, line by line; return them capitalized
                /*while (true) {
                    String input = inputStream.readLine();
                    if (input == null || input.isEmpty()) {
                        break;
                    }
                    System.out.println(input.toUpperCase());
                }*/
                //productAutomaton = (ProductAutomaton) inputStream.readObject();
                /*String strjing = (String) inputStream.readObject();
                productAutomaton = gson.fromJson(strjing, ProductAutomaton.class);*/
            } catch (IOException | ClassNotFoundException e) {
                System.out.println("Error handling client #" + clientNumber);
            } finally {
                try { socket.close(); } catch (IOException e) {}
                System.out.println("Connection with client # " + clientNumber + " closed");
            }
        }
    }

